'use strict'

import { assert } from 'chai'

import path from 'path'
import fs from 'fs'
import { ledger as log, logerr } from '../src/index'

describe('logerr', () => {
  let errLog = `${__dirname}/err.log`

  it('should create a err.log file in the parent script\'s directory', (done) => {
    logerr('hello world')
    fs.stat(errLog, (err, stats) => {
      assert(!err, 'there was no error in stat\'ing')
      assert.isTrue(stats.isFile(), 'the log file exists')
      done()
    })
  })
  it('should append data to err.log when called again', (done) => {
    logerr('test')
    fs.readFile(errLog, 'utf8', (err, data) => {
      assert(!err, 'there was no error reading the log file')
      assert.include(data, 'hello world', 'contains correct data')
      assert.include(data, 'test', 'contains correct data')
      done()
    })
  })
  it('should be able to be suppressed by calling ledger._suppress()', () => {
    log._suppress()
    logerr('blah')
    fs.readFile(errLog, 'utf8', (err, data) => {
      assert(!err, 'there was no error reading the log file')
      assert.notInclude(data, 'blah', 'doesn\'t contain data')
      done()
    })
  })
  it('should be able to be un-suppressed by calling ledger._suppress(false)', () => {
    log._suppress(false)
    logerr('blah')
    fs.readFile(errLog, 'utf8', (err, data) => {
      assert(!err, 'there was no error reading the log file')
      assert.include(data, 'blah', 'contains correct data')
      done()
    })
  })
})
describe('ledger', () => {
  let myLog = `${__dirname}/myLog`

  it('takes an argument and creates a .log file of that name in the parent script\'s directory', (done) => {
    log('myLog1')
    fs.stat(`${myLog}1.log`, (err, stats) => {
      assert(!err, 'there was no error in stat\'ing')
      assert.isTrue(stats.isFile(), 'the log file exists')
      done()
    })
  })
  describe('multiple log file support', () => {
    it('creates the files...', (done) => {
      log('myLog2')
      log('myLog3')
      log('myLog4')
      fs.stat(`${__dirname}/myLog2.log`, (err, stats) => {
        assert(!err, 'there was no error')
        assert.isTrue(stats.isFile(), 'is file')
        fs.stat(`${__dirname}/myLog3.log`, (err, stats) => {
          assert(!err, 'there was no error')
          assert.isTrue(stats.isFile(), 'is file')
          fs.stat(`${__dirname}/myLog4.log`, (err, stats) => {
            assert(!err, 'there was no error')
            assert.isTrue(stats.isFile(), 'is file')
            done()
          })
        })
      }) 
    })
    it('manages writes to the files...', (done) => {
      log.myLog2('hello myLog2')
      fs.readFile(`${myLog}2.log`, 'utf8', (err, data) => {
        assert(!err, 'there was no error reading the log file')
        assert.include(data, 'hello myLog2', 'contains correct data')
        log.myLog3('hello myLog3')
        fs.readFile(`${myLog}3.log`, 'utf8', (err, data) => {
          assert(!err, 'there was no error reading the log file')
          assert.include(data, 'hello myLog3', 'contains correct data')
          done()
        })
      })
    })
  })
  describe('log file', () => {
    it('should be accessible via dot notation on the loggerer namespace', () => {
      assert.ok(log.myLog1, 'myLog exists on loggerer namespace')
    })
    it('should be a function which accepts data to write to the file', (done) => {
      log.myLog1('hello world')
      fs.readFile(`${myLog}1.log`, 'utf8', (err, data) => {
        assert(!err, 'there was no error reading the log file')
        assert.include(data, 'hello world', 'contains correct data')
        done()
      })
    })
    describe('"suppress" feature', () => {
      it('should be able to suppress log appending by calling ledger._suppress()', () => {
        log._suppress()
        log.myLog1('blah')
        fs.readFile(`${myLog}1.log`, 'utf8', (err, data) => {
          assert(!err, 'there was no error reading the log file')
          assert.notInclude(data, 'blah', 'doesn\'t contain data')
          done()
        })
      })
      it('should be able to be un-suppressed by calling ledger._suppress(false)', () => {
        log._suppress(false)
        log.myLog1('blah')
        fs.readFile(`${myLog}1.log`, 'utf8', (err, data) => {
          assert(!err, 'there was no error reading the log file')
          assert.include(data, 'blah', 'contains correct data')
          done()
        })
      })
    })
    describe('log.myLog.reset()', () => {
      it('should clear the contents of the log file', (done) => {
        log.myLog1.reset()
        fs.readFile(`${myLog}1.log`, 'utf8', (err, data) => {
          assert(!err, 'there was no error reading file')
          assert.strictEqual(data, '', 'file is empty')
          done()
        })
      })
    })
  })
})
