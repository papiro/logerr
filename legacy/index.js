'use strict'

import fs from 'fs'
import path from 'path'
import { Console } from 'console'

let parentDir = path.dirname(module.parent.filename)
,   suppress = false

let logerr = (data) => {
  let consoleErr = new Console(process.stdout, fs.createWriteStream(`${parentDir}/err.log`)).error
   
  // re-assign logerr() on the first call
  logerr = (data) => {
    if( !suppress ) {
      consoleErr(data)    // log to err.log
      console.error(data) // log to stderr
    }
  }
  logerr(data)
}

let ledger = (log_name, debug) => {
  let fqlog_name = path.resolve(parentDir, log_name) + '.log'
  ,   newLog = new log(fqlog_name, debug)

  // make append the default function call
  ledger[log_name] = log.prototype.append.bind(newLog)

  // connect all prototype methods onto the log reference
  Object.getOwnPropertyNames(log.prototype).forEach(property => {
    ledger[log_name][property] = log.prototype[property].bind(newLog)
  })

  // no need to expose the constructor
  delete ledger[log_name].constructor
}

ledger._suppress = (toggle = true) => { suppress = toggle }

// used internally to manage the logs
class log {
  constructor(fqlog_name, debug) {
    this.logs = []

    this.logs.push(new Console( fs.createWriteStream(fqlog_name) ).log) 
    // if debug, also log to stdout
    if( debug ) this.logs.push(console.log)
    this.path = fqlog_name
  }
  
  append(data) {
    if( !suppress ) this.logs.forEach(log => log(data))
  }

  reset() {
    fs.writeFileSync(this.path, '', 'utf8')
  }
}

export { ledger, logerr }
